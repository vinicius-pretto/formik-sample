import React from "react";
import './App.css';

import Form from '../Form';

class App extends React.Component {
  render() {
    return (
      <div>
        <header>
          <h1>Formik Sample</h1>
        </header>
  
        <main className="container">
          <Form />
        </main>
      </div>
    );
  }
}

export default App;
