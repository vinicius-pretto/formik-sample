import React from "react";

const Input = (props) => {
  const { label, id, type, name, value, placeholder, error, touched, onChange, onBlur } = props;
  const hasError = error && touched;

  return (
    <div className="form__group">
      <label className={hasError ? 'label__error' : ''} htmlFor={id}>{label}</label>
      <input
        className={hasError ? 'input__error' : 'input'}
        id={id}
        type={type}
        name={name}
        value={value}
        placeholder={placeholder}
        onChange={onChange}
        onBlur={onBlur}
      />
      {hasError && <div className="error">{error}</div>}
    </div>
  );
};

export default Input;
