import * as yup from "yup";

const validationSchema = yup.object().shape({
  name: yup
    .string()
    .min(3, "Name must be at least 3 characters")
    .max(15, "Name must be at most 15 characters")
    .required("Name is required"),
  email: yup
    .string()
    .email("Must enter a valid email")
    .required("Email is required")
});

export default validationSchema;
