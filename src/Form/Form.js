import React from "react";
import { Formik } from "formik";
import Input from "./Input";
import validationSchema from './validationSchema';

class Form extends React.Component {
  initialValues = {
    name: "",
    email: ""  
  }

  onSubmit = (values, _props) => {
    alert(JSON.stringify(values, null, 2));
  };

  render() {
    return (
      <Formik
        initialValues={this.initialValues}
        onSubmit={this.onSubmit}
        validationSchema={validationSchema}
      >
        {props => {
          const {
            values,
            errors,
            touched,
            handleChange,
            handleSubmit,
            handleBlur
          } = props;

          return (
            <form onSubmit={handleSubmit}>
              <h2>Form</h2>
              <Input
                id="name"
                name="name"
                type="text"
                label="Name"
                placeholder="Name"
                value={values.name}
                error={errors.name}
                touched={touched.name}
                onChange={handleChange}
                onBlur={handleBlur}
              />
              <Input
                id="email"
                name="email"
                type="email"
                label="Email"
                placeholder="Email"
                value={values.email}
                error={errors.email}
                touched={touched.email}
                onChange={handleChange}
                onBlur={handleBlur}
              />
              <button type="submit">
                Submit
              </button>
            </form>
          );
        }}
      </Formik>
    );
  }
}

export default Form;
