# Formik Sample

## References:

* [Create React App](https://facebook.github.io/create-react-app/)
* [Formik](https://jaredpalmer.com/formik/docs/overview)
* [Yup](https://github.com/jquense/yup)

## Running Application

### Install dependencies

```
$ npm install
```

### Start App

```
$ npm start
```
